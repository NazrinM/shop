import '../../ui/layouts/layout'
import '../../ui/pages/product/add'
import '../../ui/pages/home/home'

FlowRouter.route('/', {
    action: function(params) {
        BlazeLayout.render("mainLayout", {main: 'home'});
    }
});

FlowRouter.route('/add-product', {
    action: function(params) {
        BlazeLayout.render("mainLayout", {main: 'addProduct'});
    }
});
